package com.project.armanopc.ohmedicine;

import com.firebase.client.Firebase;

/**
 * Created by ArmanoPC on 27/10/2559.
 */

public class MyApplication extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Firebase.setAndroidContext(this);
    }
}