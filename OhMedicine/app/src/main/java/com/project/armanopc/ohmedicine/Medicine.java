package com.project.armanopc.ohmedicine;


/**
 * Created by ArmanoPC on 28/10/2559.
 */

public class Medicine {
    public String name;
    public String description;
    public String type;
    public String eatTime ;
    public String morning;
    public String noon;
    public String evening;
    public String night;
    public String minute;
    public String image;


    public Medicine() {

    }

    public Medicine(String name, String description, String type, String eatTime, String morning,
                    String noon, String evening, String night, String image, String minute) {
        this.name = name;
        this.description = description;
        this.type = type;
        this.eatTime = eatTime;
        this.morning = morning;
        this.noon = noon;
        this.evening = evening;
        this.night = night;
        this.image = image;
        this.minute = minute;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEatTime() {
        return eatTime;
    }

    public void setEatTime(String eatTime) {
        this.eatTime = eatTime;
    }

    public String getMorning() {
        return morning;
    }

    public void setMorning(String morning) {
        this.morning = morning;
    }

    public String getNoon() {
        return noon;
    }

    public void setNoon(String noon) {
        this.noon = noon;
    }

    public String getEvening() {
        return evening;
    }

    public void setEvening(String evening) {
        this.evening = evening;
    }

    public String getNight() {
        return night;
    }

    public void setNight(String night) {
        this.night = night;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
