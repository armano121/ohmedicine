package com.project.armanopc.ohmedicine;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;


import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by ArmanoPC on 21/10/2559.
 */

public class OneFragment extends Fragment {

    Firebase mRootRef;
    private FirebaseAuth auth;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private DatabaseReference mDatabaseMedicine;
    public String id;
    private TextView textName, textType;
    private ImageView mphoto;
    ArrayList<Medicine> medicineList = new ArrayList<>();

    public static OneFragment newInstance(String id, String data) {
        OneFragment fragment = new OneFragment();
        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        bundle.putString("data", data);
        fragment.setArguments(bundle);
        return fragment;
    }

    public OneFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_one, container, false);

        mRootRef = new Firebase("https://ohmedicine.firebaseio.com/");
        //get firebase auth instance
        auth = FirebaseAuth.getInstance();
        textName = (TextView) rootView.findViewById(R.id.textone);
        textType = (TextView) rootView.findViewById(R.id.texttwo);
        mphoto = (ImageView) rootView.findViewById(R.id.medicine_image);

        id = getArguments().getString("id");
        String user_id = auth.getCurrentUser().getUid();

        mDatabaseMedicine = FirebaseDatabase.getInstance().getReference().child("test").child(user_id).child(id);

        loadNameMedicine();


        return rootView;
    }

    public void loadNameMedicine() {
        Log.i("getid", id);

        final ArrayList<String> medicineListName = new ArrayList<>();

//        mDatabaseMedicine.child(user_id).child(id).addChildEventListener(new com.google.firebase.database.ChildEventListener() {
//            @Override
//            public void onChildAdded(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {
//                Log.i("data", dataSnapshot.getValue().toString());
//                Medicine medicine = dataSnapshot.getValue(Medicine.class);
//                Map<String, Object> medicinevalue = (Map<String,Object>) dataSnapshot.getValue();
////                String med_name = medicinevalue.get("name").toString();
////                Log.i("valuename", med_name);
//                medicineList.add(medicine);
//                Log.i("medname",medicine.name);
//
////             textName.setText(medicine.minute);
////                medicineListName.add(medicine.name);
//
//
//                int listsize = medicineList.size();
//
//                for (int i = 0;i<listsize;i++){
//
//                    textName.setText(medicineList.get(i).name);
//
//                    Log.i("test",medicineList.get(i).name);
//
//                }
//            }
//
//            @Override
//            public void onChildChanged(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onChildRemoved(com.google.firebase.database.DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
        mDatabaseMedicine.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Medicine medicine = dataSnapshot.getValue(Medicine.class);
                medicineList.add(medicine);
                showName(medicine);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    @Override
    public void onStart() {
        super.onStart();

    }

    public void showName(Medicine medicine) {

            textName.setText(medicine.name);
            textType.setText(medicine.type);
            Picasso.with(getApplicationContext()).load(medicine.image).resize(150, 150).into(mphoto);

   //     }

    }
}
