package com.project.armanopc.ohmedicine;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by ArmanoPC on 21/10/2559.
 */

public class PageAdapter extends FragmentPagerAdapter {

    ArrayList<OneFragment> list = new ArrayList<>();

    public PageAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(OneFragment fragment) {
        list.add(fragment);
        Log.i("adapter","count :"+list.size());
        notifyDataSetChanged();
    }

    public int getCount() {
        return list.size();
    }

    public OneFragment getItem(int position) {
        return list.get(position);
    }

}
