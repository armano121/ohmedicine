package com.project.armanopc.ohmedicine;

import android.app.Dialog;
import android.app.FragmentManager;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceFragment;

import android.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.phenotype.Configuration;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;

/*
 * Created by ArmanoPC on 4/10/2559.
 */

public class AddMedicineActivity extends AppCompatActivity {

    private static String TAG = AddMedicineActivity.class.getSimpleName();
    ListView mDrawerList;
    RelativeLayout mDrawerPane;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;

    private ToggleButton pillBtn, potionBtn, eatBeforeBtn, eatAfterBtn, withMealBtn,
            fifteenBtn, thirtyBtn, morningBtn, noonBtn, eveningBtn, nightBtn;
    private ImageButton addImageBtn;
    private EditText medicineName,medicineDESC;
    private Button submitBtn;
    private static final int GALLERY_REQUEST = 1;
    private Uri mImageUri = null;
    private StorageReference mStorage;
    private static final int TIME_DIALOG_ID = 0;

    private int mHour;
    private int mMinute;

    String medicine_type;
    String medicine_eatTime;
    String medicine_minutes;
    String medicine_morning;
    String medicine_noon;
    String medicine_evening;
    String medicine_night;

    private DatabaseReference mDatabaseMedicine;
    private FirebaseAuth auth;

    ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addmedicine);

        auth = FirebaseAuth.getInstance();
        mStorage = FirebaseStorage.getInstance().getReference();

        pillBtn = (ToggleButton) findViewById(R.id.pill);
        potionBtn = (ToggleButton) findViewById(R.id.potion);
        addImageBtn = (ImageButton) findViewById(R.id.add_picture);
        medicineName = (EditText) findViewById(R.id.medicine_name);
        medicineDESC = (EditText) findViewById(R.id.medicine_desc);
        submitBtn = (Button) findViewById(R.id.add_medicine);
        eatBeforeBtn = (ToggleButton) findViewById(R.id.eat_before);
        eatAfterBtn = (ToggleButton) findViewById(R.id.eat_after);
        fifteenBtn = (ToggleButton) findViewById(R.id.fifteen_min);
        thirtyBtn = (ToggleButton) findViewById(R.id.thirty_min);
        withMealBtn = (ToggleButton) findViewById(R.id.eat_with);
        morningBtn = (ToggleButton) findViewById(R.id.time_morning);
        noonBtn = (ToggleButton) findViewById(R.id.time_afternoon);
        eveningBtn = (ToggleButton) findViewById(R.id.time_evening);
        nightBtn = (ToggleButton) findViewById(R.id.time_night);

        mDatabaseMedicine = FirebaseDatabase.getInstance().getReference().child("Medicine");

        addImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, GALLERY_REQUEST);

            }
        });

        pillBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pillBtn.isChecked() == true ){

                    potionBtn.setChecked(false);
                    Toast.makeText(AddMedicineActivity.this, pillBtn.getTextOff().toString(),
                            Toast.LENGTH_SHORT).show();
                    medicine_type = pillBtn.getText().toString().trim();

                }

            }
        });

        potionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (potionBtn.isChecked() == true ) {

                    pillBtn.setChecked(false);
                    Toast.makeText(AddMedicineActivity.this, potionBtn.getTextOff().toString(),
                            Toast.LENGTH_SHORT).show();
                    medicine_type = potionBtn.getText().toString().trim();

                }

            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addMedicine();

            }
        });

        eatBeforeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (eatBeforeBtn.isChecked() == true){

                    eatAfterBtn.setChecked(false);
                    withMealBtn.setChecked(false);

                    fifteenBtn.setVisibility(View.VISIBLE);
                    thirtyBtn.setVisibility(View.VISIBLE);
                    medicine_eatTime = eatBeforeBtn.getHint().toString().trim();

                } else {

                    fifteenBtn.setVisibility(View.INVISIBLE);
                    thirtyBtn.setVisibility(View.INVISIBLE);

                }
            }
        });

        eatAfterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (eatAfterBtn.isChecked() == true ){

                    eatBeforeBtn.setChecked(false);
                    withMealBtn.setChecked(false);

                    fifteenBtn.setVisibility(View.VISIBLE);
                    thirtyBtn.setVisibility(View.VISIBLE);
                    medicine_eatTime = eatAfterBtn.getHint().toString().trim();

                }else {

                    fifteenBtn.setVisibility(View.INVISIBLE);
                    thirtyBtn.setVisibility(View.INVISIBLE);

                }

            }
        });

        withMealBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                eatBeforeBtn.setChecked(false);
                eatAfterBtn.setChecked(false);

                fifteenBtn.setVisibility(View.INVISIBLE);
                thirtyBtn.setVisibility(View.INVISIBLE);
                medicine_eatTime = withMealBtn.getHint().toString().trim();

            }
        });

        fifteenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (fifteenBtn.isChecked() == true ){

                    thirtyBtn.setChecked(false);
                    medicine_minutes = fifteenBtn.getTextOff().toString();

                }

            }
        });

        thirtyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (thirtyBtn.isChecked() == true ){

                    fifteenBtn.setChecked(false);
                    medicine_minutes = thirtyBtn.getTextOff().toString().trim();

                }

            }
        });

        morningBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                showDialog(TIME_DIALOG_ID);
                morningBtn.setChecked(true);
                medicine_morning = pad(mHour) + ":" + pad(mMinute);

                return true;

            }
        });

        morningBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mHour = 8;
                mMinute = 00;
                medicine_morning = pad(mHour) + ":" + pad(mMinute);

            }
        });

        noonBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                showDialog(TIME_DIALOG_ID);
                noonBtn.setChecked(true);
                medicine_noon = pad(mHour) + ":" + pad(mMinute);

                return true;
            }
        });

        noonBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mHour = 12;
                mMinute = 00;
                medicine_noon = pad(mHour) + ":" + pad(mMinute);

            }
        });

        eveningBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                showDialog(TIME_DIALOG_ID);
                eveningBtn.setChecked(true);
                medicine_evening = pad(mHour) + ":" + pad(mMinute);

                return true;
            }
        });

        eveningBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mHour = 16;
                mMinute = 00;
                medicine_evening = pad(mHour) + ":" + pad(mMinute);

            }
        });

        nightBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                showDialog(TIME_DIALOG_ID);
                nightBtn.setChecked(true);
                medicine_night = pad(mHour) + ":" + pad(mMinute);

                return true;
            }
        });

        nightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mHour = 16;
                mMinute = 00;
                medicine_night = pad(mHour) + ":" + pad(mMinute);

            }
        });

//        mNavItems.add(new NavItem("Home", "Your Medicine", R.drawable.ic_action_home));
//        mNavItems.add(new NavItem("Setting", "Change you setting", R.drawable.ic_action_gear));
//
//     //Slide menu
//        // DrawerLayout
//        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
//
//        // Populate the Navigtion Drawer with options
//        mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);
//        mDrawerList = (ListView) findViewById(R.id.navList);
//        DrawerListAdapter adapter = new DrawerListAdapter(this, mNavItems);
//        mDrawerList.setAdapter(adapter);
//
//        // Drawer Item click listeners
//        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
////                selectItemFromDrawer(position);
//            }
//        });
//
//        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
//
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//
//                invalidateOptionsMenu();
//            }
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//                Log.d(TAG, "onDrawerClosed: " + getTitle());
//
//                invalidateOptionsMenu();
//            }
//
//        };
//
//        mDrawerLayout.setDrawerListener(mDrawerToggle);
//        //end slide menu

    }

    //Slide menu
//
//    private void selectItemFromDrawer(int position) {
//
//        PreferencesFragement preferencesFragement = new PreferencesFragement();
//
//        FragmentManager fragmentManager = getFragmentManager();
//        fragmentManager.beginTransaction()
//                .replace(R.id.mainContent, preferencesFragement)
//                .commit();
//
//        mDrawerList.setItemChecked(position, true);
//        setTitle(mNavItems.get(position).mTitle);
//
//        //Close the drawer
//        mDrawerLayout.closeDrawer(mDrawerPane);
//
//    }
    // end Slide menu

    @Override
    protected Dialog onCreateDialog(int id) {
        switch(id) {
            case TIME_DIALOG_ID:
                return new TimePickerDialog(this,
                        mTimeSetListener, mHour, mMinute, false);

        }
        return null;

    }

    private void updateDisplay(){
        morningBtn.setTextOn(
                new StringBuilder().append("Morning \n")
                .append(pad(mHour)).append(":")
                .append(pad(mMinute))
        );

        noonBtn.setTextOn(
                new StringBuilder().append("Noon \n")
                        .append(pad(mHour)).append(":")
                        .append(pad(mMinute))
        );

        eveningBtn.setTextOn(
                new StringBuilder().append("Evening \n")
                        .append(pad(mHour)).append(":")
                        .append(pad(mMinute))
        );

        nightBtn.setTextOn(
                new StringBuilder().append("Night \n")
                        .append(pad(mHour)).append(":")
                        .append(pad(mMinute))
        );

    }

    private TimePickerDialog.OnTimeSetListener mTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int hourofday, int minute){

                    mHour = hourofday;
                    mMinute = minute;
                    updateDisplay();

                }
            };

    private static String pad(int c) {
        String zero = "0";
        if (c >= 10)
            return String.valueOf(c);
        else
            return zero + String.valueOf(c);
    }

    public void addMedicine(){

        final String medicine_name = medicineName.getText().toString().trim();
        final String medicine_desc = medicineDESC.getText().toString().trim();
        StorageReference filepath = mStorage.child("Medicine_image").child(mImageUri.getLastPathSegment());

        if (!TextUtils.isEmpty(medicine_name)){

            String user_id = auth.getCurrentUser().getUid();
            final DatabaseReference medicinePost = mDatabaseMedicine.child(user_id).push();

            medicinePost.child("Medicine_name").setValue(medicine_name);
            medicinePost.child("Medicine_desc").setValue(medicine_desc);
            medicinePost.child("Medicine_type").setValue(medicine_type);
            medicinePost.child("Medicine_eatTime").setValue(medicine_eatTime);
            medicinePost.child("Medicine_minutes").setValue(medicine_minutes);
            medicinePost.child("Medicine_morning").setValue(medicine_morning);
            medicinePost.child("Medicine_noon").setValue(medicine_noon);
            medicinePost.child("Medicine_evening").setValue(medicine_evening);
            medicinePost.child("Medicine_night").setValue(medicine_night);

            filepath.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    Uri downloadUri = taskSnapshot.getDownloadUrl();
                    medicinePost.child("Medicine_image").setValue(downloadUri.toString());

                    Toast.makeText(AddMedicineActivity.this, "Success",
                            Toast.LENGTH_SHORT).show();

                }
            });

        }

    }

    //Set imageuri
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){

            mImageUri = data.getData();

            addImageBtn.setImageURI(mImageUri);
        }
    }
    //End set imageuri

}