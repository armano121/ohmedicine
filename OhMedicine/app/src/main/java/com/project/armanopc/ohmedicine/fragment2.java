package com.project.armanopc.ohmedicine;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by ArmanoPC on 19/10/2559.
 */

public class fragment2 extends Fragment {

    public fragment2(){

        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment2, container, false);

        getFragmentManager().beginTransaction().remove(fragment2.this).commit();

        return rootView;

    }
}
