package com.project.armanopc.ohmedicine;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Map;

public class MainActivity extends ActionBarActivity {

    private static String TAG = AddMedicineActivity.class.getSimpleName();
    ListView mDrawerList;
    RelativeLayout mDrawerPane;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    ViewPager paper;
    private TextView profileText, emailText, bigProfileText;
    private ImageView profileImage, bigProfileImage;
    private PageAdapter pageAdapter;
    private Boolean exit = false;
    View drawerView;


    ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();

    Firebase mRootRef;
    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    FirebaseUser user;
    private DatabaseReference mDatabase;
    private DatabaseReference mDatabaseMedicine;
    private String user_id;
    Firebase Profilename;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mLockScreenRotation();

        profileText = (TextView) findViewById(R.id.user_name);
        profileImage = (ImageView) findViewById(R.id.avatar);
        emailText = (TextView) findViewById(R.id.sub_text);
        bigProfileImage = (ImageView) findViewById(R.id.imageprofile);
        bigProfileText = (TextView) findViewById(R.id.pen);

        mRootRef = new Firebase("https://ohmedicine.firebaseio.com/");
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Register");
        mDatabase.keepSynced(true);
        mDatabaseMedicine = FirebaseDatabase.getInstance().getReference().child("test");

        //get current user
        user = FirebaseAuth.getInstance().getCurrentUser();

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    // user auth state is changed - user is null
                    // launch login activity
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };
        //get firebase auth instance
        auth = FirebaseAuth.getInstance();

        //get user id
        user_id = auth.getCurrentUser().getUid();

        //get Register information
        Profilename = mRootRef.child("Register").child(user_id);

        //check user
        checkUserExist();

//        if (user != null) {
//
//            String name = user.getDisplayName();
//            String email = user.getEmail();
//            Uri photoUrl = user.getPhotoUrl();
//
////            profileText.setText(name);
//            Picasso.with(getApplicationContext()).load(photoUrl).fit().into(profileImage);
//
//        }

        //Slide menu
        mNavItems.add(new NavItem("Home", "Your Medicine", R.drawable.ic_action_home));
        mNavItems.add(new NavItem("Add Medicine", "Add your medicine", R.drawable.ic_action_pill));
        mNavItems.add(new NavItem("Setting", "Change you setting", R.drawable.ic_action_gear));


        // DrawerLayout
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        // Populate the Navigtion Drawer with options
        mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);
        mDrawerList = (ListView) findViewById(R.id.navList);
        DrawerListAdapter adapter = new DrawerListAdapter(this, mNavItems);
        mDrawerList.setAdapter(adapter);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        setTitle(mNavItems.get(0).getmTitle());
//        mDrawerList.setItemChecked(0, true);
//        mDrawerLayout.closeDrawer(mDrawerList);

        // Drawer Item click listeners
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItemFromDrawer(position);

                mDrawerList.setItemChecked(position, true);
                setTitle(mNavItems.get(position).getmTitle());

                //Close the drawer
                mDrawerLayout.closeDrawer(mDrawerPane);

            }
        });

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Log.d(TAG, "onDrawerClosed: " + getTitle());

                invalidateOptionsMenu();
            }

        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        //end slide menu

        //Load user Data
//        loadUserData();

        Profilename.addValueEventListener(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                Map<String, String> map = dataSnapshot.getValue(Map.class);
                String name = map.get("Username");
                String image = map.get("Image");
                String email = map.get("Email");
                Uri photoUrl = user.getPhotoUrl();

                profileText.setText(name);
                emailText.setText(email);
                bigProfileText.setText(name);

                if (image != null) {

                    Picasso.with(getApplicationContext()).load(image).fit().into(profileImage);
                    Picasso.with(getApplicationContext()).load(image).resize(200, 200).into(bigProfileImage);

                } else {
                    Picasso.with(getApplicationContext()).load(photoUrl).fit().into(profileImage);
                    Picasso.with(getApplicationContext()).load(photoUrl).resize(200, 200).into(bigProfileImage);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        pageAdapter = new PageAdapter(getSupportFragmentManager());
        paper = (ViewPager) findViewById(R.id.pager);
        paper.setAdapter(pageAdapter);
        loadMedicine();
    }
    // End onCreate

    public void mLockScreenRotation() {

        switch (this.getResources().getConfiguration().orientation) {
            case Configuration.ORIENTATION_PORTRAIT:
                this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
        }
    }

    public void loadMedicine() {

        String user_id = auth.getCurrentUser().getUid();
        Log.i("Key", user_id);
        mDatabaseMedicine.child(user_id).addChildEventListener(new com.google.firebase.database.ChildEventListener() {
            @Override
            public void onChildAdded(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {
                Log.i("data", dataSnapshot.getValue().toString());
                Medicine medicine = dataSnapshot.getValue(Medicine.class);
              //  Gson gson = new Gson();
                pageAdapter.addFragment(OneFragment.newInstance(dataSnapshot.getKey(),s));
//                Map<String, Object> medicinevalue = (Map<String,Object>) dataSnapshot.getValue();
//                String med_name = medicinevalue.get("name").toString();
//                Log.i("valuename", med_name);

            }

            @Override
            public void onChildChanged(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(com.google.firebase.database.DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();


    }

//    public void loadUserData(){
//
//        final String user_id = auth.getCurrentUser().getUid();
//        final Firebase Profilename = mRootRef.child("Register").child(user_id);
//        Profilename.addValueEventListener(new com.firebase.client.ValueEventListener() {
//            @Override
//            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
//                Map<String, String> map = dataSnapshot.getValue(Map.class);
//                String name = map.get("Username");
//                String image = map.get("Image");
//                String email = map.get("Email");
//                Uri photoUrl = user.getPhotoUrl();
//
//                profileText.setText(name);
//                emailText.setText(email);
//
//                if (image != null) {
//
//                    Picasso.with(getApplicationContext()).load(image).fit().into(profileImage);
//
//                } else {
//                    Picasso.with(getApplicationContext()).load(photoUrl).fit().into(profileImage);
//                }
//            }
//
//            @Override
//            public void onCancelled(FirebaseError firebaseError) {
//
//            }
//        });
//    }

    //Slide menu

    private void selectItemFromDrawer(int pos) {
        Fragment fragment = null;

        switch (pos) {
            case 0:
                fragment = new fragment2();
                break;
            case 1:
                fragment = new Fragment_addMedicine();
                break;
            case 2:
                fragment = new PreferencesFragement();
                break;
            default:
                fragment = new fragment2();
                break;
        }

        if (null != fragment) {

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.mainContent, fragment);
            transaction.addToBackStack(null);
            transaction.commit();

        }

//        FragmentManager fragmentManager = getFragmentManager();
//        fragmentManager.beginTransaction()
//                .replace(R.id.mainContent, fragment)
//                .commit();

    }
    // end Slide menu

    private void checkUserExist() {

        if (auth.getCurrentUser() != null) {

            final String user_id = auth.getCurrentUser().getUid();

            mDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if (!dataSnapshot.hasChild(user_id)) {

                        Intent intentsetup = new Intent(MainActivity.this, EditdataActivity.class);
                        intentsetup.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intentsetup);

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }

    }

    @Override
    public void onBackPressed() {
        mDrawerLayout.closeDrawer(mDrawerPane);

        if (exit == false) {
            mDrawerLayout.closeDrawer(mDrawerPane);
//            finish(); // finish activity
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }
    }

}

class NavItem {
    String mTitle;
    String mSubtitle;
    int mIcon;

    public NavItem(String title, String subtitle, int icon) {
        mTitle = title;
        mSubtitle = subtitle;
        mIcon = icon;
    }

    public int getmIcon() {
        return mIcon;
    }

    public void setmIcon(int mIcon) {
        this.mIcon = mIcon;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmSubtitle() {
        return mSubtitle;
    }

    public void setmSubtitle(String mSubtitle) {
        this.mSubtitle = mSubtitle;
    }
}

class DrawerListAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<NavItem> mNavItems;

    public DrawerListAdapter(Context context, ArrayList<NavItem> navItems) {
        mContext = context;
        mNavItems = navItems;
    }

    @Override
    public int getCount() {
        return mNavItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mNavItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
//        View view;
//
//        if (convertView == null){
//            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            view = inflater.inflate(R.layout.drawer_item, null);
//        }else{
//            view = convertView;
//        }

        View v = View.inflate(mContext, R.layout.drawer_item, null);

        TextView titleView = (TextView) v.findViewById(R.id.title);
        TextView subtitleView = (TextView) v.findViewById(R.id.subTitle);
        ImageView iconView = (ImageView) v.findViewById(R.id.icon);

        titleView.setText(mNavItems.get(position).mTitle);
        subtitleView.setText(mNavItems.get(position).mSubtitle);
        iconView.setImageResource(mNavItems.get(position).mIcon);

        return v;
    }
}


