package com.project.armanopc.ohmedicine;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.DrawableRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by ArmanoPC on 29/9/2559.
 */

public class EditdataActivity extends AppCompatActivity {

    private EditText user_name;
    private TextView name_display;
    private Button saveBtn, signoutBtn;
    private DatabaseReference mDatabase;
    private StorageReference mStorage;
    private ImageView selectImage;
    private static final int GALLERY_REQUEST = 1;
    private static final int CAMERA_CAPTURE = 2;
    private static int CROP_PIC = 2;
    private Uri mImageUri = null;
    private FirebaseAuth auth;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    String uri = "@drawable/marijuana";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        mLockScreenRotation();

        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance();

        name_display = (TextView) findViewById(R.id.display_name);
        user_name = (EditText) findViewById(R.id.user_name);
        saveBtn = (Button) findViewById(R.id.save_button);
        signoutBtn = (Button) findViewById(R.id.sign_out);
        mStorage = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Register");
        selectImage = (ImageView) findViewById(R.id.btn_image);

//        int imageResource = getResources().getIdentifier(uri, null, getPackageName());
//
//        Drawable res = getResources().getDrawable(imageResource);
//        selectImage.setImageDrawable(res);

        if (user != null) {
            // Name, email address, and profile photo Url
            String name = user.getDisplayName();
            String email = user.getEmail();
            Uri photoUrl = user.getPhotoUrl();

            name_display.setText(name);

            user_name.setText(name);

            updateImage(photoUrl);

            Picasso.with(getApplicationContext()).load(photoUrl).resize(200,200).into(selectImage);

        }

//        selectImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
//                galleryIntent.setType("image/*");
//                startActivityForResult(galleryIntent, GALLERY_REQUEST);
//
//            }
//        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startPosting();

            }
        });

//        signoutBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                signOut();
//            }
//        });



    }

    public void mLockScreenRotation(){

        switch (this.getResources().getConfiguration().orientation){
            case Configuration.ORIENTATION_PORTRAIT:
                this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
        }
    }

    public void updateImage(Uri uri) {
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent.setData(uri);
        sendBroadcast(intent);
    }

    //----->> Start Post method
    private void startPosting(){

        final String input_val = user_name.getText().toString().trim();
        final String user_id = auth.getCurrentUser().getUid();

        if(!TextUtils.isEmpty(input_val) ){

//            StorageReference filepath = mStorage.child("Profile_image").child(mImageUri.getLastPathSegment());
            DatabaseReference newPost = mDatabase.child(user_id);

            newPost.child("Username").setValue(input_val);
            newPost.child("Email").setValue(FirebaseAuth.getInstance().getCurrentUser().getEmail());

            startActivity(new Intent(EditdataActivity.this, MainActivity.class));


//            // Check put file success
//            filepath.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                @Override
//                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//
//                    String downloadUri = taskSnapshot.getDownloadUrl().toString();
//
//                    newPost.child("Image").setValue(downloadUri);
//
////                    mProgress.dismiss();
//
//                }
//            });

        }

    }
    //------>> End Post method

    //Set imageuri
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){

            mImageUri = data.getData();

            selectImage.setImageURI(mImageUri);
        }

//        if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
//
//            if (requestCode == GALLERY_REQUEST) {
//                // get the Uri for the captured image
//                //mImageUri = data.getData();
//                performCrop();
//            }
//            // user is returning from cropping the image
//            else if (requestCode == CROP_PIC) {
//
//                Toast toast = Toast
//                        .makeText(this, "crop complete", Toast.LENGTH_SHORT);
//                toast.show();
//                // get the returned data
//                Bundle extras = data.getExtras();
//                // get the cropped bitmap
//                Bitmap thePic = extras.getParcelable("data");
//                selectImage.setImageBitmap(thePic);
//            }
//        }

//            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            startActivityForResult(intent,CAMERA_CAPTURE);

//                Uri imageUri = data.getData();
//            File file = new File(Environment.getExternalStorageDirectory()+"/ohMedicine");
//            file.mkdirs();
//            File fileName = new File(Environment.getExternalStorageDirectory()+"/ohMedicine/image01.jpg");
//                    CropImage.activity(imageUri)
//                    .setGuidelines(CropImageView.Guidelines.ON)
//                    .setAspectRatio(1, 1)
//                           // .setOutputUri(Uri.fromFile(fileName))
//                    .start(this);

           // selectImage.setImageURI(Uri.fromFile(fileName));

//            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//                CropImage.ActivityResult result = CropImage.getActivityResult(data);
//                if (resultCode == RESULT_OK) {
//
//                    Log.e("data",data.toString());
//                    mImageUri = result.getUri();
//
//                    selectImage.setImageURI(mImageUri);
//
//                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//
//                    Exception error = result.getError();
//               error.printStackTrace();
//                }
//            }

    }
    //End set imageuri

//    private void performCrop() {
//        // take care of exceptions
//        try {
//            // call the standard crop action intent (the user device may not
//            // support it)
//            Intent cropIntent = new Intent("com.android.camera.action.CROP");
//            // indicate image type and Uri
//            cropIntent.setDataAndType(mImageUri, "image/*");
//            // set crop properties
//            cropIntent.putExtra("crop", "true");
//            // indicate aspect of desired crop
//            cropIntent.putExtra("aspectX", 1);
//            cropIntent.putExtra("aspectY", 1);
//            // indicate output X and Y
//            cropIntent.putExtra("outputX", 256);
//            cropIntent.putExtra("outputY", 256);
//            // retrieve data on return
//            cropIntent.putExtra("return-data", true);
//            // start the activity - we handle returning in onActivityResult
//            startActivityForResult(cropIntent, CROP_PIC);
//        }
//        // respond to users whose devices do not support the crop action
//        catch (ActivityNotFoundException anfe) {
//            Toast toast = Toast
//                    .makeText(this, "This device doesn't support the crop action!", Toast.LENGTH_SHORT);
//            toast.show();
//        }
//    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        signOut();
    }

    private void signOut() {
        // Firebase sign out
        auth.signOut();

        LoginManager.getInstance().logOut();

        startActivity(new Intent(EditdataActivity.this, LoginActivity.class));
        finish();
    }


}
