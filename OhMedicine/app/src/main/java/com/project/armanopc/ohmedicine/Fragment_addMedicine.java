package com.project.armanopc.ohmedicine;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by ArmanoPC on 20/10/2559.
 */

public class Fragment_addMedicine extends Fragment {

    private static String TAG = AddMedicineActivity.class.getSimpleName();
    ListView mDrawerList;
    RelativeLayout mDrawerPane;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;

    private ToggleButton pillBtn, potionBtn, eatBeforeBtn, eatAfterBtn, withMealBtn,
            fifteenBtn, thirtyBtn, morningBtn, noonBtn, eveningBtn, nightBtn;
    private ImageButton addImageBtn;
    private EditText medicineName, medicineDESC;
    private Button submitBtn;
    private static final int GALLERY_REQUEST = 1;
    public static final int RESULT_OK = -1;
    private Uri mImageUri = null;
    private StorageReference mStorage;
    private static final int TIME_DIALOG_ID = 0;

    private int mHour = 8;
    private int mMinute = 00;
    private int nHour = 12;
    private int nMinute = 00;
    private int eHour = 16;
    private int eMinute = 00;
    private int gnHour = 20;
    private int gnMinute = 00;

    String medicine_type;
    String medicine_eatTime;
    String medicine_minutes;
    String medicine_morning;
    String medicine_noon;
    String medicine_evening;
    String medicine_night;
    String medicine_image;

    private DatabaseReference mDatabaseMedicine;
    private FirebaseAuth auth;
    private DatabaseReference mDatabase;

    ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();
    private TimePickerDialog timePickerDialog=null;

    public Fragment_addMedicine() {

        //Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_addmedicine, container, false);

        auth = FirebaseAuth.getInstance();
        mStorage = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        final String user_id = auth.getCurrentUser().getUid();

        pillBtn = (ToggleButton) rootView.findViewById(R.id.pill);
        potionBtn = (ToggleButton) rootView.findViewById(R.id.potion);
        addImageBtn = (ImageButton) rootView.findViewById(R.id.add_picture);
        medicineName = (EditText) rootView.findViewById(R.id.medicine_name);
        medicineDESC = (EditText) rootView.findViewById(R.id.medicine_desc);
        submitBtn = (Button) rootView.findViewById(R.id.add_medicine);
        eatBeforeBtn = (ToggleButton) rootView.findViewById(R.id.eat_before);
        eatAfterBtn = (ToggleButton) rootView.findViewById(R.id.eat_after);
        fifteenBtn = (ToggleButton) rootView.findViewById(R.id.fifteen_min);
        thirtyBtn = (ToggleButton) rootView.findViewById(R.id.thirty_min);
        withMealBtn = (ToggleButton) rootView.findViewById(R.id.eat_with);
        morningBtn = (ToggleButton) rootView.findViewById(R.id.time_morning);
        noonBtn = (ToggleButton) rootView.findViewById(R.id.time_afternoon);
        eveningBtn = (ToggleButton) rootView.findViewById(R.id.time_evening);
        nightBtn = (ToggleButton) rootView.findViewById(R.id.time_night);

        mDatabaseMedicine = FirebaseDatabase.getInstance().getReference().child("Medicine");

        addImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, GALLERY_REQUEST);

            }
        });

        pillBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pillBtn.isChecked() == true) {

                    potionBtn.setChecked(false);
                    Toast.makeText(getActivity(), pillBtn.getTextOff().toString(),
                            Toast.LENGTH_SHORT).show();
                    medicine_type = pillBtn.getText().toString().trim();

                } else {
                    medicine_type = null;
                }

            }
        });

        potionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (potionBtn.isChecked() == true) {

                    pillBtn.setChecked(false);
                    Toast.makeText(getActivity(), potionBtn.getTextOff().toString(),
                            Toast.LENGTH_SHORT).show();
                    medicine_type = potionBtn.getText().toString().trim();

                } else {
                    medicine_type = null;
                }

            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String medicine_name = medicineName.getText().toString().trim();
                String medicine_desc = medicineDESC.getText().toString().trim();

                if (mImageUri == null ){
                    Toast.makeText(getActivity(), "Please Enter The Image!",
                            Toast.LENGTH_SHORT).show();
                }

                if(medicine_type == null){
                    Toast.makeText(getActivity(), "Please Enter The Medicine Type!",
                            Toast.LENGTH_SHORT).show();
                }

                if(TextUtils.isEmpty(medicine_name)){
                    Toast.makeText(getActivity(), "Please Enter The Medicine Name!",
                            Toast.LENGTH_SHORT).show();
                }

                if(TextUtils.isEmpty(medicine_desc)){
                    Toast.makeText(getActivity(), "Please Enter The Medicine Description!",
                            Toast.LENGTH_SHORT).show();
                }

                if (medicine_name != null && medicine_desc != null && medicine_image != null && medicine_type != null ){

                    if (medicine_morning != null || medicine_noon != null || medicine_evening != null || medicine_night != null){
                        if(medicine_eatTime != null){
                            if(medicine_minutes != null || medicine_eatTime == withMealBtn.getHint().toString().trim()){
                                writeNewMedicine(user_id , medicine_name, medicine_desc,
                                        medicine_type, medicine_eatTime, medicine_morning, medicine_noon,
                                        medicine_evening, medicine_night, medicine_image, medicine_minutes);
                            }else{
//                                writeNewMedicine(user_id , medicine_name, medicine_desc,
//                                        medicine_type, medicine_eatTime, medicine_morning, medicine_noon,
//                                        medicine_evening, medicine_night, medicine_image, medicine_minutes);
                                Toast.makeText(getActivity(), "Please Enter Minute Time at least Once!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(getActivity(), "Please Enter Meal Time at least Once!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else{
                        Toast.makeText(getActivity(), "Please Enter Time at least Once!",
                                Toast.LENGTH_SHORT).show();
                    }
                }
//                writeNewMedicine(user_id , medicine_name, medicine_desc,
//                        medicine_type, medicine_eatTime, medicine_morning, medicine_noon,
//                        medicine_evening, medicine_night, medicine_image, medicine_minutes);
            }
        });

        eatBeforeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (eatBeforeBtn.isChecked() == true) {

                    eatAfterBtn.setChecked(false);
                    withMealBtn.setChecked(false);

                    fifteenBtn.setVisibility(View.VISIBLE);
                    thirtyBtn.setVisibility(View.VISIBLE);
                    medicine_eatTime = eatBeforeBtn.getHint().toString().trim();

                } else {
                    fifteenBtn.setChecked(false);
                    thirtyBtn.setChecked(false);
                    fifteenBtn.setVisibility(View.INVISIBLE);
                    thirtyBtn.setVisibility(View.INVISIBLE);
                    medicine_eatTime = null;

                }
            }
        });

        eatAfterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (eatAfterBtn.isChecked() == true) {

                    eatBeforeBtn.setChecked(false);
                    withMealBtn.setChecked(false);

                    fifteenBtn.setVisibility(View.VISIBLE);
                    thirtyBtn.setVisibility(View.VISIBLE);
                    medicine_eatTime = eatAfterBtn.getHint().toString().trim();

                } else {
                    fifteenBtn.setChecked(false);
                    thirtyBtn.setChecked(false);
                    fifteenBtn.setVisibility(View.INVISIBLE);
                    thirtyBtn.setVisibility(View.INVISIBLE);
                    medicine_eatTime = null;

                }

            }
        });

        withMealBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (withMealBtn.isChecked() == true){
                    eatBeforeBtn.setChecked(false);
                    eatAfterBtn.setChecked(false);
                    fifteenBtn.setChecked(false);
                    thirtyBtn.setChecked(false);
                    medicine_minutes = null;

                    fifteenBtn.setVisibility(View.INVISIBLE);
                    thirtyBtn.setVisibility(View.INVISIBLE);
                    medicine_eatTime = withMealBtn.getHint().toString().trim();
                }else{
                    medicine_eatTime = null;
                }

            }
        });

        fifteenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (fifteenBtn.isChecked() == true) {

                    thirtyBtn.setChecked(false);
                    medicine_minutes = fifteenBtn.getTextOff().toString();

                }else{
                    medicine_minutes = null;
                }

            }
        });

        thirtyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (thirtyBtn.isChecked() == true) {

                    fifteenBtn.setChecked(false);
                    medicine_minutes = thirtyBtn.getTextOff().toString().trim();

                }else{
                    medicine_minutes = null;
                }

            }
        });

        morningBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                MshowDialog(TIME_DIALOG_ID);
                if (morningBtn.isChecked() == true ){
                    medicine_morning = pad(mHour) + ":" + pad(mMinute);
                    Log.i("kuy1",medicine_morning);

                }else{
                    medicine_morning = null;
                }
                return true;

            }
        });

        morningBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(morningBtn.isChecked() == true){
                    medicine_morning = pad(mHour) + ":" + pad(mMinute);
                    Toast.makeText(getActivity(), "Morning "+pad(mHour)+":"+pad(mMinute),
                            Toast.LENGTH_SHORT).show();
                } else {
                    medicine_morning = null;
                }

            }
        });

        noonBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                NshowDialog(TIME_DIALOG_ID);
                if(noonBtn.isChecked() == true){
                    medicine_noon = pad(nHour) + ":" + pad(nMinute);
                }else{
                    medicine_noon = null;
                }

                return true;
            }
        });

        noonBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(noonBtn.isChecked() == true){
                    medicine_noon = pad(nHour) + ":" + pad(nMinute);
                    Toast.makeText(getActivity(), "Noon "+pad(nHour)+":"+pad(nMinute),
                            Toast.LENGTH_SHORT).show();
                }else{
                    medicine_noon = null;
                }

            }
        });

        eveningBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                EshowDialog(TIME_DIALOG_ID);
                if (eveningBtn.isChecked() == true ){
                    medicine_evening = pad(eHour) + ":" + pad(eMinute);
                }else {
                    medicine_evening = null;
                }

                return true;
            }
        });

        eveningBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (eveningBtn.isChecked() == true ){
                    medicine_evening = pad(eHour) + ":" + pad(eMinute);
                    Toast.makeText(getActivity(), "Evening "+pad(eHour)+":"+pad(eMinute),
                            Toast.LENGTH_SHORT).show();
                }else {
                    medicine_evening = null;
                }

            }
        });

        nightBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                GNshowDialog(TIME_DIALOG_ID);
                if (nightBtn.isChecked() == true ){
                    medicine_night = pad(gnHour) + ":" + pad(gnMinute);
                }else {
                    medicine_night = null;
                }

                return true;
            }
        });

        nightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (nightBtn.isChecked() == true ){
                    medicine_night = pad(gnHour) + ":" + pad(gnMinute);
                    Toast.makeText(getActivity(), "Night "+pad(gnHour)+":"+pad(gnMinute),
                            Toast.LENGTH_SHORT).show();
                }else {
                    medicine_night = null;
                }

            }
        });

        return rootView;
    }

    /////////////////////////////Start Time picker ////////////////////////////////////
    protected TimePickerDialog MonCreateDialog(int id) {
        switch (id) {
            case TIME_DIALOG_ID:
                timePickerDialog = new TimePickerDialog(getActivity(), mTimeSetListener, mHour, mMinute, true);
                timePickerDialog.show();
                return timePickerDialog;

        }
        return null;

    }

    protected TimePickerDialog NonCreateDialog(int id) {
        switch (id) {
            case TIME_DIALOG_ID:
                timePickerDialog = new TimePickerDialog(getActivity(), nTimeSetListener, nHour, nMinute, true);
                timePickerDialog.show();
                return timePickerDialog;

        }
        return null;

    }

    protected TimePickerDialog EonCreateDialog(int id) {
        switch (id) {
            case TIME_DIALOG_ID:
                timePickerDialog = new TimePickerDialog(getActivity(), eTimeSetListener, eHour, eMinute, true);
                timePickerDialog.show();
                return timePickerDialog;

        }
        return null;

    }

    protected TimePickerDialog GNonCreateDialog(int id) {
        switch (id) {
            case TIME_DIALOG_ID:
                timePickerDialog = new TimePickerDialog(getActivity(), gnTimeSetListener, gnHour, gnMinute, true);
                timePickerDialog.show();
                return timePickerDialog;

        }
        return null;

    }
    private void morningUpdate(){
        medicine_morning = pad(mHour) + ":" + pad(mMinute);
        morningBtn.setTextOn(
                new StringBuilder().append("Morning \n")
                        .append(medicine_morning)

        );
        morningBtn.setTextOff(
                new StringBuilder().append("Morning \n")
                        .append(pad(mHour)).append(":")
                        .append(pad(mMinute))

        );
        morningBtn.setChecked(true);
        Toast.makeText(getActivity(), "Morning "+pad(mHour)+":"+pad(mMinute),
                Toast.LENGTH_SHORT).show();
    }
    private void noonUpdate(){
        medicine_noon = pad(nHour) + ":" + pad(nMinute);
        noonBtn.setTextOn(
                new StringBuilder().append("Noon \n")
                        .append(medicine_noon)
        );
        noonBtn.setTextOff(
                new StringBuilder().append("Noon \n")
                        .append(pad(nHour)).append(":")
                        .append(pad(nMinute))
        );
        noonBtn.setChecked(true);
        Toast.makeText(getActivity(), "Noon "+pad(nHour)+":"+pad(nMinute),
                Toast.LENGTH_SHORT).show();
    }
    private void eveningUpdate(){
        medicine_evening = pad(eHour) + ":" + pad(eMinute);
        eveningBtn.setTextOn(
                new StringBuilder().append("Evening \n")
                        .append(medicine_evening)
        );
        eveningBtn.setTextOff(
                new StringBuilder().append("Evening \n")
                        .append(pad(eHour)).append(":")
                        .append(pad(eMinute))
        );
        eveningBtn.setChecked(true);
        Toast.makeText(getActivity(), "Evening "+pad(eHour)+":"+pad(eMinute),
                Toast.LENGTH_SHORT).show();
    }
    private void nightUpdate(){
        medicine_night = pad(gnHour) + ":" + pad(gnMinute);
        nightBtn.setTextOn(
                new StringBuilder().append("Night \n")
                        .append(medicine_night)
        );
        nightBtn.setTextOff(
                new StringBuilder().append("Night \n")
                        .append(pad(gnHour)).append(":")
                        .append(pad(gnMinute))
        );
        nightBtn.setChecked(true);
        Toast.makeText(getActivity(), "Night "+pad(gnHour)+":"+pad(gnMinute),
                Toast.LENGTH_SHORT).show();
    }

    private TimePickerDialog.OnTimeSetListener mTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int hourofday, int minute) {

                    mHour = hourofday;
                    mMinute = minute;
                    morningUpdate();
                }
            };

    private TimePickerDialog.OnTimeSetListener nTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int hourofday, int minute) {

                    nHour = hourofday;
                    nMinute = minute;
                    noonUpdate();

                }
            };

    private TimePickerDialog.OnTimeSetListener eTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int hourofday, int minute) {

                    eHour = hourofday;
                    eMinute = minute;
                    eveningUpdate();

                }
            };

    private TimePickerDialog.OnTimeSetListener gnTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int hourofday, int minute) {

                    gnHour = hourofday;
                    gnMinute = minute;
                    nightUpdate();

                }
            };

    private static String pad(int c) {
        String zero = "0";
        if (c >= 10)
            return String.valueOf(c);
        else
            return zero + String.valueOf(c);
    }

    @Deprecated
    public final void MshowDialog(int id) {

        MonCreateDialog(id);

    }

    @Deprecated
    public final void NshowDialog(int id) {

        NonCreateDialog(id);

    }

    @Deprecated
    public final void EshowDialog(int id) {

        EonCreateDialog(id);

    }

    @Deprecated
    public final void GNshowDialog(int id) {

        GNonCreateDialog(id);

    }

    //////////////////////////////////////End Time picker//////////////////////////

//    public void addMedicine() {
//
//        final String medicine_name = medicineName.getText().toString().trim();
//        final String medicine_desc = medicineDESC.getText().toString().trim();
//
//
//        if (!TextUtils.isEmpty(medicine_name)) {
//
//            String user_id = auth.getCurrentUser().getUid();
//            final DatabaseReference medicinePost = mDatabaseMedicine.child(user_id).push();
//
//            medicinePost.child("Medicine_name").setValue(medicine_name);
//            medicinePost.child("Medicine_desc").setValue(medicine_desc);
//            medicinePost.child("Medicine_type").setValue(medicine_type);
//            medicinePost.child("Medicine_eatTime").setValue(medicine_eatTime);
//            medicinePost.child("Medicine_minutes").setValue(medicine_minutes);
//            medicinePost.child("Medicine_morning").setValue(medicine_morning);
//            medicinePost.child("Medicine_noon").setValue(medicine_noon);
//            medicinePost.child("Medicine_evening").setValue(medicine_evening);
//            medicinePost.child("Medicine_night").setValue(medicine_night);
//            //HashMap<String, Object> objectHashMap = new HashMap<>();
//            mDatabaseMedicine.child(user_id).addChildEventListener(new ChildEventListener() {
//                @Override
//                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                    Log.i("data",dataSnapshot.toString());
//                    Medicine medicine = dataSnapshot.getValue(Medicine.class);
//                    Log.i("getvalue", medicine.toString());
//                }
//
//                @Override
//                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//
//                }
//
//                @Override
//                public void onChildRemoved(DataSnapshot dataSnapshot) {
//
//                }
//
//                @Override
//                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//
//                }
//            });
//
//            if (mImageUri != null){
//                StorageReference filepath = mStorage.child("Medicine_image").child(mImageUri.getLastPathSegment());
//
//                filepath.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                    @Override
//                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//
//                        Uri downloadUri = taskSnapshot.getDownloadUrl();
//                        medicinePost.child("Medicine_image").setValue(downloadUri.toString());
//
//                        startActivity(new Intent(getActivity(), MainActivity.class));
//
//                        Toast.makeText(getActivity(), "Success",
//                                Toast.LENGTH_SHORT).show();
//
//                    }
//                });
//            }
//
//        }
//
//    }

    public void writeNewMedicine(String userId, String name,
                             String desc, String type,
                             String eatTime, String morning,
                             String noon, String evening,
                             String night, String image, String minute) {
        Medicine addMeddicine = new Medicine(name, desc, type, eatTime,
                morning, noon, evening, night, image, minute);

        mDatabase.child("test").child(userId).push().setValue(addMeddicine).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getActivity(), "Success",
                        Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getActivity(), MainActivity.class));
            }
        });

    }

    //Set imageuri
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {

            mImageUri = data.getData();

            addImageBtn.setImageURI(mImageUri);
        }

        if (mImageUri != null){
            StorageReference filepath = mStorage.child("Medicine_image").child(mImageUri.getLastPathSegment());

            filepath.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    Uri downloadUri = taskSnapshot.getDownloadUrl();
                    medicine_image = downloadUri.toString();

                }
            });
        }
    }
    //End set imageuri


}
